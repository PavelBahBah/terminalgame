package ru.kpe.game;

import ru.kpe.combatants.Combatant;
import ru.kpe.combatants.enemies.Enemy;
import ru.kpe.combatants.heroes.Hero;
import ru.kpe.states.StateList;

import java.util.ArrayList;
import java.util.List;

public class Battle {

    private List<Enemy> enemies;
    private List<Combatant> combatantsOrder;
    private Integer currentOrder = 0;
    private Combatant currentCombatant;
    private List<BattleLogAction> battleLog = new ArrayList<>();

    public Battle(List<Enemy> enemies) {
        this.enemies = new ArrayList<>();
        this.enemies.addAll(enemies);
        combatantsOrder = new ArrayList<>();
        combatantsOrder.addAll(World.game.getSquad().getHeroes());
        combatantsOrder.addAll(enemies);
        currentCombatant = combatantsOrder.get(currentOrder);
    }

    public void nextPhase() {
        processLastBattleLogAction();
        removeDeadCombatants();
        if (enemies.isEmpty()) {
            finishBattle();
        }
        currentOrder++;
        if (currentOrder == combatantsOrder.size()) {
            currentOrder = 0;
        }
        currentCombatant = combatantsOrder.get(currentOrder);
        if (Enemy.class.isAssignableFrom(currentCombatant.getClass())) {
            processEnemyTurn();
        }
    }

    public BattleLogAction getLastBattleLogAction() {
        return battleLog.get(battleLog.size() - 1);
    }

    private void processLastBattleLogAction() {
        BattleLogAction action = battleLog.get(battleLog.size() - 1);
        if (BattleLogAction.BattleLogActionType.ABILITY.equals(action.getActionType())) {
            action.getAbility().useAbility(action.getSource(), action.getTarget());
        }
        action.setCurrentToStringState(action.toString());
    }

    private void removeDeadCombatants() {
        List<Combatant> removeCandidates = new ArrayList<>();
        for (Combatant combatant: combatantsOrder) {
            if (combatant.getHp() <= 0) {
                if (Enemy.class.isAssignableFrom(combatant.getClass())) {
                    enemies.remove(combatant);
                }
                if (Hero.class.isAssignableFrom(combatant.getClass())) {
                    World.game.getSquad().getHeroes().remove(combatant);
                }
                removeCandidates.add(combatant);
            }
        }
        combatantsOrder.removeAll(removeCandidates);
    }

    private void finishBattle() {
        World.game.setState(StateList.IN_LOCATION);
    }

    private void processEnemyTurn() {
        ((Enemy) currentCombatant).processTurn();
        nextPhase();
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public List<Combatant> getCombatantsOrder() {
        return combatantsOrder;
    }

    public void setCombatantsOrder(List<Combatant> combatantsOrder) {
        this.combatantsOrder = combatantsOrder;
    }

    public Integer getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Integer currentOrder) {
        this.currentOrder = currentOrder;
    }

    public Combatant getCurrentCombatant() {
        return currentCombatant;
    }

    public void setCurrentCombatant(Combatant currentCombatant) {
        this.currentCombatant = currentCombatant;
    }

    public List<BattleLogAction> getBattleLog() {
        return battleLog;
    }

    public void setBattleLog(List<BattleLogAction> battleLog) {
        this.battleLog = battleLog;
    }
}
