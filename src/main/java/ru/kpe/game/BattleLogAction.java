package ru.kpe.game;

import ru.kpe.abilities.Ability;
import ru.kpe.abilities.AbilityList;
import ru.kpe.combatants.Combatant;
import ru.kpe.items.Item;

public class BattleLogAction {

    private Combatant source;
    private BattleLogActionType actionType;
    private Ability ability;
    private Item item;
    private Combatant target;
    private String currentToStringState;

    public BattleLogAction(Combatant source, BattleLogActionType actionType, Combatant target, Object actionObject) {
        this.source = source;
        this.actionType = actionType;
        this.target = target;
        if (actionType.equals(BattleLogActionType.ABILITY)) {
            ability = ((Ability) actionObject);
        }
        if (actionType.equals(BattleLogActionType.ITEM)) {
            item = ((Item) actionObject);
        }
    }

    public enum BattleLogActionType {
        ABILITY,
        ITEM
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(source.getCombatantInfo());
        if (actionType.equals(BattleLogActionType.ABILITY)) {
            if (AbilityList.SKIP.equals(ability)) {
                stringBuilder.append(" пропускает ход.");
                return stringBuilder.toString();
            }
            stringBuilder.append(" использует способность ").append(ability.getAbilityTitle());
        }
        if (actionType.equals(BattleLogActionType.ITEM)) {
            stringBuilder.append(" использует предмет ").append(item.getItemTitle());
        }
        if (target != null) {
            stringBuilder.append(" на цель ").append(target.getCombatantInfo());
        }
        return stringBuilder.toString();
    }

    public Combatant getSource() {
        return source;
    }

    public void setSource(Combatant source) {
        this.source = source;
    }

    public BattleLogActionType getActionType() {
        return actionType;
    }

    public void setActionType(BattleLogActionType actionType) {
        this.actionType = actionType;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Combatant getTarget() {
        return target;
    }

    public void setTarget(Combatant target) {
        this.target = target;
    }

    public String getCurrentToStringState() {
        return currentToStringState;
    }

    public void setCurrentToStringState(String currentToStringState) {
        this.currentToStringState = currentToStringState;
    }
}
