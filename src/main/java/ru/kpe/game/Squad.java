package ru.kpe.game;

import ru.kpe.combatants.heroes.Hero;
import ru.kpe.locations.Location;

import java.util.List;

public class Squad {

    private List<Hero> heroes;
    private Inventory inventory;
    private Location location;

    public Squad(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
