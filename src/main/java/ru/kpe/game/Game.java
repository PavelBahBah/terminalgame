package ru.kpe.game;

import ru.kpe.npcs.NPC;
import ru.kpe.states.State;

public class Game {

    private State state;
    private NPC interactedNPC;
    private Squad squad;
    private Battle battle;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public NPC getInteractedNPC() {
        return interactedNPC;
    }

    public void setInteractedNPC(NPC interactedNPC) {
        this.interactedNPC = interactedNPC;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squad) {
        this.squad = squad;
    }

    public Battle getBattle() {
        return battle;
    }

    public void setBattle(Battle battle) {
        this.battle = battle;
    }
}
