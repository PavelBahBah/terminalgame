package ru.kpe;

import ru.kpe.game.Inventory;
import ru.kpe.game.Game;
import ru.kpe.game.Squad;
import ru.kpe.game.World;
import ru.kpe.items.HealthPotion;
import ru.kpe.items.ManaPotion;
import ru.kpe.locations.LocationList;
import ru.kpe.combatants.heroes.Healer;
import ru.kpe.combatants.heroes.Warrior;
import ru.kpe.states.StateList;

import java.util.Arrays;
import java.util.Scanner;

import static ru.kpe.util.ConsoleUtil.*;

public class Application {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        init();
        clearConsole();
        while (true) {
            clearConsole();
            showInfo();
            processConsoleInput();
        }
    }

    private static void init() {
        World.game = new Game();
        World.game.setSquad(new Squad(Arrays.asList(
                new Healer(),
                new Warrior()
        )));
        World.game.getSquad().setLocation(LocationList.START_LOCATION);
        World.game.setState(StateList.IN_LOCATION);
        World.game.getSquad().setInventory(new Inventory());
        World.game.getSquad().getInventory().getItems().add(new HealthPotion());
        World.game.getSquad().getInventory().getItems().add(new ManaPotion());
    }

    private static void processConsoleInput() {
        System.out.println("====================");
        World.game.getState().showStateActions();
        System.out.println("====================");
        System.out.print("Выберите действие: ");

        String input = scanner.next();
        World.game.getState().doAction(Integer.parseInt(input));

    }

    private static void showInfo() {
        World.game.getState().showStateInfo();
    }

}
