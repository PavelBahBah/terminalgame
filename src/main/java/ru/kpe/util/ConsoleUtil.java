package ru.kpe.util;

import java.io.IOException;

public final class ConsoleUtil {

    private ConsoleUtil() { }

    public static void clearConsole() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (InterruptedException | IOException ignored) {}
    }

}
