package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class CloseInventoryAction extends Action {

    @Override
    public String getActionTitle() {
        return "Закрыть инвентарь";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.IN_LOCATION);
    }
}
