package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class UseItemAction extends Action {
    @Override
    public String getActionTitle() {
        return "Использовать предмет";
    }

    @Override
    public void doAction() {
        World.game.getSquad().getInventory().getChosenItem().useItem();
        World.game.getSquad().getInventory().setChosenItem(null);
        World.game.setState(StateList.IN_INVENTORY);
    }
}
