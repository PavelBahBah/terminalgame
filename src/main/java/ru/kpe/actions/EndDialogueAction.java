package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class EndDialogueAction extends Action {
    @Override
    public String getActionTitle() {
        return "Завершить общение";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.NPC_CHOOSING);
    }
}
