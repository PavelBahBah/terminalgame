package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.items.Item;
import ru.kpe.states.StateList;

public class DropItemAction extends Action {

    @Override
    public String getActionTitle() {
        return "Выбросить предмет";
    }

    @Override
    public void doAction() {
        Item chosenItem = World.game.getSquad().getInventory().getChosenItem();
        World.game.getSquad().getInventory().getItems().remove(chosenItem);
        World.game.getSquad().getInventory().setChosenItem(null);
        World.game.setState(StateList.IN_INVENTORY);
    }
}
