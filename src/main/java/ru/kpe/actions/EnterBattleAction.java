package ru.kpe.actions;

import ru.kpe.combatants.enemies.Skeleton;
import ru.kpe.game.Battle;
import ru.kpe.game.World;
import ru.kpe.states.StateList;

import java.util.Arrays;

public class EnterBattleAction extends Action {

    @Override
    public String getActionTitle() {
        return "Вступить в битву";
    }

    @Override
    public void doAction() {
        World.game.setBattle(new Battle(Arrays.asList(new Skeleton(), new Skeleton())));
        World.game.setState(StateList.BATTLE);
    }
}
