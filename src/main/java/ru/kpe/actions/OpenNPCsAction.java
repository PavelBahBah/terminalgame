package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class OpenNPCsAction extends Action {
    @Override
    public String getActionTitle() {
        return "NPC";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.NPC_CHOOSING);
    }
}
