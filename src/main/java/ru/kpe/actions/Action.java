package ru.kpe.actions;

public abstract class Action {

    public abstract String getActionTitle();
    public abstract void doAction();

}
