package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class OpenInventoryAction extends Action {
    @Override
    public String getActionTitle() {
        return "Инвентарь";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.IN_INVENTORY);
    }
}
