package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class OpenAbilitiesAction extends Action {
    @Override
    public String getActionTitle() {
        return "Способности";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.ABILITY_CHOOSING);
    }
}
