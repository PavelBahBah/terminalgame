package ru.kpe.actions;

public final class ActionList {

    private ActionList() { }

    public static final Action OPEN_INVENTORY = new OpenInventoryAction();
    public static final Action OPEN_LOCATION = new OpenLocationsAction();
    public static final Action OPEN_NPC = new OpenNPCsAction();
    public static final Action CLOSE_INVENTORY = new CloseInventoryAction();
    public static final Action END_DIALOGUE = new EndDialogueAction();
    public static final Action USE_ITEM = new UseItemAction();
    public static final Action DROP_ITEM = new DropItemAction();
    public static final Action ENTER_BATTLE = new EnterBattleAction();
    public static final Action OPEN_ABILITIES = new OpenAbilitiesAction();

}
