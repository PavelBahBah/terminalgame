package ru.kpe.actions;

import ru.kpe.game.World;
import ru.kpe.states.StateList;

public class OpenLocationsAction extends Action {
    @Override
    public String getActionTitle() {
        return "Локации";
    }

    @Override
    public void doAction() {
        World.game.setState(StateList.LOCATION_CHOOSING);
    }
}
