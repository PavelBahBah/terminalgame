package ru.kpe.states;

import ru.kpe.game.World;
import ru.kpe.npcs.NPC;

import java.util.List;

public class NPCChoosingState extends State {

    @Override
    public void showStateInfo() {
        List<NPC> NPCs = World.game.getSquad().getLocation().getNPCs();
        System.out.println("0 - Назад");
        for (int i = 0; i < NPCs.size(); i++) {
            System.out.println((i + 1) + " - " + NPCs.get(i).getNPCName());
        }
    }

    @Override
    public void doAction(Integer choice) {
        if (choice == 0) {
            World.game.setState(StateList.IN_LOCATION);
            return;
        }
        List<NPC> NPSs = World.game.getSquad().getLocation().getNPCs();
        World.game.setInteractedNPC(NPSs.get(choice - 1));
        World.game.setState(StateList.DIALOGUE);
    }
}
