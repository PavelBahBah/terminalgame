package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.actions.ActionList;
import ru.kpe.actions.EnterBattleAction;
import ru.kpe.game.World;

import java.util.Map;

public class InLocationState extends State {

    public InLocationState(Action... actions) {
        super(actions);
    }

    @Override
    public void showStateActions() {
        for (Map.Entry<Integer, Action> entry: getAvailableActions().entrySet()) {
            if (ActionList.ENTER_BATTLE.equals(entry.getValue()) && !World.game.getSquad().getLocation().isBattleLocation()) {
                continue;
            }
            System.out.println(entry.getKey() + " - " + entry.getValue().getActionTitle());
        }
    }

    @Override
    public void showStateInfo() {
        System.out.println("Вы сейчас находитесь в: " + World.game.getSquad().getLocation().getLocationTitle());
    }

}
