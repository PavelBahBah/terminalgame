package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.combatants.enemies.Enemy;
import ru.kpe.game.BattleLogAction;
import ru.kpe.game.World;
import ru.kpe.combatants.heroes.Hero;

import java.util.List;
import java.util.stream.Collectors;

public class BattleState extends State {

    public BattleState(Action... actions) {
        super(actions);
    }

    @Override
    public void showStateInfo() {
        List<BattleLogAction> battleLog = World.game.getBattle().getBattleLog();
        String battleLogInfo;
        if (battleLog.size() > 0) {
            if (battleLog.size() <= 5) {
                battleLogInfo = battleLog.stream()
                        .map(BattleLogAction::getCurrentToStringState).collect(Collectors.joining("\n"));
            } else {
                battleLogInfo = battleLog.stream().skip(battleLog.size() - 5)
                        .map(BattleLogAction::getCurrentToStringState).collect(Collectors.joining("\n"));
            }
            System.out.println(battleLogInfo);
            System.out.println("=====");
        }
        System.out.println("Битва против: ");
        for (Enemy enemy: World.game.getBattle().getEnemies()) {
            System.out.println(enemy.getCombatantInfo());
        }
    }

    @Override
    public void showStateActions() {
        for (Hero hero : World.game.getSquad().getHeroes()) {
            if (World.game.getBattle().getCurrentCombatant().equals(hero)) {
                System.out.print("> ");
            }
            System.out.println(hero.getCombatantInfo());
        }
        super.showStateActions();
    }
}
