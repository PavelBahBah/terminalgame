package ru.kpe.states;

import ru.kpe.game.BattleLogAction;
import ru.kpe.combatants.Combatant;
import ru.kpe.game.World;

import java.util.ArrayList;
import java.util.List;

public class TargetChoosingState extends State {

    @Override
    public void showStateInfo() {
        System.out.println("Выберите цель: ");
    }

    // TODO: Добавить цветовую индикацию союзников/противников
    @Override
    public void showStateActions() {
        List<Combatant> combatants = new ArrayList<>();
        BattleLogAction action = World.game.getBattle().getLastBattleLogAction();
        if (BattleLogAction.BattleLogActionType.ABILITY.equals(action.getActionType())) {
            if (action.getAbility().canBeCastedOnAlly()) {
                combatants.addAll(World.game.getSquad().getHeroes());
            }
            if (action.getAbility().canBeCastedOnEnemy()) {
                combatants.addAll(World.game.getBattle().getEnemies());
            }
        }
        System.out.println("0 - Назад");
        for (int i = 0; i < combatants.size(); i++) {
            System.out.println((i + 1) + " - " + combatants.get(i).getCombatantInfo());
        }
    }

    @Override
    public void doAction(Integer choice) {
        BattleLogAction action = World.game.getBattle().getLastBattleLogAction();
        if (choice == 0) {
            World.game.setState(StateList.ABILITY_CHOOSING);
            World.game.getBattle().getBattleLog().remove(action);
            return;
        }
        List<Combatant> combatants = new ArrayList<>();
        if (BattleLogAction.BattleLogActionType.ABILITY.equals(action.getActionType())) {
            if (action.getAbility().canBeCastedOnAlly()) {
                combatants.addAll(World.game.getSquad().getHeroes());
            }
            if (action.getAbility().canBeCastedOnEnemy()) {
                combatants.addAll(World.game.getBattle().getEnemies());
            }
        }
        combatants.addAll(World.game.getSquad().getHeroes());
        combatants.addAll(World.game.getBattle().getEnemies());
        action.setTarget(combatants.get(choice - 1));
        World.game.setState(StateList.BATTLE);
        World.game.getBattle().nextPhase();
    }
}
