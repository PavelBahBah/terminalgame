package ru.kpe.states;

import ru.kpe.actions.ActionList;

public final class StateList {

    private StateList() { }

    public static final State IN_LOCATION = new InLocationState(
            ActionList.OPEN_LOCATION,
            ActionList.OPEN_NPC,
            ActionList.OPEN_INVENTORY,
            ActionList.ENTER_BATTLE);

    public static final State IN_INVENTORY = new InInventoryState(
            ActionList.CLOSE_INVENTORY
    );

    public static final State LOCATION_CHOOSING = new LocationChoosingState();

    public static final State NPC_CHOOSING = new NPCChoosingState();

    public static final State DIALOGUE = new DialogueState(
            ActionList.END_DIALOGUE
    );

    public static final State ITEM_ACTION_CHOOSING = new ItemActionChoosingState(
            ActionList.USE_ITEM,
            ActionList.DROP_ITEM,
            ActionList.OPEN_INVENTORY
    );

    public static final State BATTLE = new BattleState(
            ActionList.OPEN_ABILITIES
    );

    public static final State ABILITY_CHOOSING = new AbilityChoosingState();

    public static final State TARGET_CHOOSING = new TargetChoosingState();

}
