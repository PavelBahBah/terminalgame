package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.game.World;

public class DialogueState extends State {

    public DialogueState(Action... actions) {
        super(actions);
    }

    @Override
    public void showStateInfo() {
        System.out.println("Вы общаетесь с " + World.game.getInteractedNPC().getNPCName());
    }

}
