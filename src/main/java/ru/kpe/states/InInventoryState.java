package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.game.World;
import ru.kpe.items.Item;

import java.util.List;

public class InInventoryState extends State {

    public InInventoryState(Action... actions) {
        super(actions);
    }

    @Override
    public void showStateInfo() {
        System.out.println("Ваш инвентарь: бла-бла");
    }

    @Override
    public void showStateActions() {
        List<Item> items = World.game.getSquad().getInventory().getItems();
        System.out.println("0 - Назад");
        for (int i = 0; i < items.size(); i++) {
            // TODO: Добавить цвет в зав-ти от редкости
            System.out.println((i + 1) + " - " + items.get(i).getItemTitle());
        }
    }

    @Override
    public void doAction(Integer choice) {
        if (choice == 0) {
            World.game.setState(StateList.IN_LOCATION);
            return;
        }
        List<Item> items = World.game.getSquad().getInventory().getItems();
        World.game.getSquad().getInventory().setChosenItem(items.get(choice - 1));
        World.game.setState(StateList.ITEM_ACTION_CHOOSING);
    }
}
