package ru.kpe.states;

import ru.kpe.locations.Location;
import ru.kpe.game.World;

import java.util.List;

public class LocationChoosingState extends State {

    @Override
    public void showStateInfo() {
        System.out.println("Выберите локацию: ");
    }

    @Override
    public void showStateActions() {
        List<Location> locations = World.game.getSquad().getLocation().getLinkedLocations();
        System.out.println("0 - Назад");
        for (int i = 0; i < locations.size(); i++) {
            System.out.println((i + 1) + " - " + locations.get(i).getLocationTitle());
        }
    }

    @Override
    public void doAction(Integer choice) {
        if (choice == 0) {
            World.game.setState(StateList.IN_LOCATION);
            return;
        }
        List<Location> locations = World.game.getSquad().getLocation().getLinkedLocations();
        World.game.getSquad().setLocation(locations.get(choice - 1));
        World.game.setState(StateList.IN_LOCATION);
    }
}
