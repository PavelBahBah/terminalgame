package ru.kpe.states;

import ru.kpe.abilities.Ability;
import ru.kpe.game.BattleLogAction;
import ru.kpe.game.World;
import ru.kpe.combatants.heroes.Hero;

import java.util.List;

public class AbilityChoosingState extends State {

    @Override
    public void showStateInfo() {
        System.out.println("Выберите способность: ");
    }

    @Override
    public void showStateActions() {
        List<Ability> abilities = ((Hero) World.game.getBattle().getCurrentCombatant()).getAbilities();
        System.out.println("0 - Назад");
        for (int i = 0; i < abilities.size(); i++) {
            System.out.println((i + 1) + " - " + abilities.get(i).getAbilityTitle());
        }
    }

    @Override
    public void doAction(Integer choice) {
        if (choice == 0) {
            World.game.setState(StateList.BATTLE);
            return;
        }
        Ability ability = ((Hero) World.game.getBattle().getCurrentCombatant()).getAbilities().get(choice - 1);
        BattleLogAction battleLogAction = new BattleLogAction(
                World.game.getBattle().getCurrentCombatant(),
                BattleLogAction.BattleLogActionType.ABILITY,
                null,
                ability
        );
        World.game.getBattle().getBattleLog().add(battleLogAction);
        if (ability.isNonTargetAbility()) {
            World.game.setState(StateList.BATTLE);
            World.game.getBattle().nextPhase();
            return;
        }
        World.game.setState(StateList.TARGET_CHOOSING);
    }
}
