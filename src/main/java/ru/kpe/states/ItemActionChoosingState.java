package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.game.World;

public class ItemActionChoosingState extends State {

    public ItemActionChoosingState(Action... actions) {
        super(actions);
    }

    @Override
    public void showStateInfo() {
        System.out.println("Выберите что сделать с предметом: ");
    }

}
