package ru.kpe.states;

import ru.kpe.actions.Action;
import ru.kpe.game.World;

import java.util.HashMap;
import java.util.Map;

public abstract class State {

    private Map<Integer, Action> availableActions = new HashMap<>();

    public State(Action... actions) {
        for (int i = 0; i < actions.length; i++) {
            availableActions.put(i + 1, actions[i]);
        }
    }

    public abstract void showStateInfo();

    public void showStateActions() {
        for (Map.Entry<Integer, Action> entry: availableActions.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue().getActionTitle());
        }
    }

    public void doAction(Integer choice) {
        if (availableActions.get(choice) != null) {
            availableActions.get(choice).doAction();
        }
    }

    public Map<Integer, Action> getAvailableActions() {
        return availableActions;
    }
}
