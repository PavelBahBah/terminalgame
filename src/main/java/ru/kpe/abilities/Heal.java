package ru.kpe.abilities;

import ru.kpe.combatants.Combatant;

public class Heal extends Ability {
    @Override
    public String getAbilityTitle() {
        return "Лечение";
    }

    @Override
    public boolean isNonTargetAbility() {
        return false;
    }

    @Override
    public boolean canBeCastedOnAlly() {
        return true;
    }

    @Override
    public boolean canBeCastedOnEnemy() {
        return false;
    }

    @Override
    public void castOnAlly(Combatant owner, Combatant target) {
        target.setHp(target.getHp() + 5);
        if (target.getHp() > target.getMaxHp()) {
            target.setHp(target.getMaxHp());
        }
    }
}
