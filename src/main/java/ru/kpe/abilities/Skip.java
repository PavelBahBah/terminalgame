package ru.kpe.abilities;

public class Skip extends Ability {

    @Override
    public String getAbilityTitle() {
        return "Пропустить ход";
    }

    @Override
    public boolean isNonTargetAbility() {
        return true;
    }

    @Override
    public boolean canBeCastedOnAlly() {
        return false;
    }

    @Override
    public boolean canBeCastedOnEnemy() {
        return false;
    }

}
