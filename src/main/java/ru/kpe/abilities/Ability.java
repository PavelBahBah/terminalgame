package ru.kpe.abilities;

import ru.kpe.combatants.enemies.Enemy;
import ru.kpe.combatants.Combatant;
import ru.kpe.combatants.heroes.Hero;

public abstract class Ability {

    public abstract String getAbilityTitle();
    public abstract boolean isNonTargetAbility();
    public abstract boolean canBeCastedOnAlly();
    public abstract boolean canBeCastedOnEnemy();

    public void castOnEnemy(Combatant source, Combatant target) {

    }

    public void castOnAlly(Combatant source, Combatant target) {

    }

    public void castNonTarget(Combatant source) {

    }

    public void useAbility(Combatant source, Combatant target) {
        if (isNonTargetAbility()) {
            castNonTarget(source);
            return;
        }
        if (Hero.class.isAssignableFrom(source.getClass())) {
            if (Enemy.class.isAssignableFrom(target.getClass())) {
                castOnEnemy(source, target);
            }
            if (Hero.class.isAssignableFrom(target.getClass())) {
                castOnAlly(source, target);
            }
        }
        if (Enemy.class.isAssignableFrom(source.getClass())) {
            if (Hero.class.isAssignableFrom(target.getClass())) {
                castOnEnemy(source, target);
            }
            if (Enemy.class.isAssignableFrom(target.getClass())) {
                castOnAlly(source, target);
            }
        }
    }
}
