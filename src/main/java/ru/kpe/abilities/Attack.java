package ru.kpe.abilities;

import ru.kpe.combatants.Combatant;

public class Attack extends Ability {

    @Override
    public String getAbilityTitle() {
        return "Атака";
    }

    @Override
    public boolean isNonTargetAbility() {
        return false;
    }

    @Override
    public boolean canBeCastedOnAlly() {
        return false;
    }

    @Override
    public boolean canBeCastedOnEnemy() {
        return true;
    }

    @Override
    public void castOnEnemy(Combatant owner, Combatant target) {
        target.setHp(target.getHp() - 5);
    }

}
