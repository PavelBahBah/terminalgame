package ru.kpe.abilities;

public final class AbilityList {

    private AbilityList() {}

    public static final Ability ATTACK = new Attack();
    public static final Ability HEAL = new Heal();
    public static final Ability SKIP = new Skip();

}
