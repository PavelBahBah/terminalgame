package ru.kpe.items;

import ru.kpe.game.World;

public class HealthPotion extends Item {

    @Override
    public void useItem() {
        removeItself();
    }

    @Override
    public String getItemTitle() {
        return "Зелье здоровья";
    }

    @Override
    public Rarity getItemRarity() {
        return Rarity.COMMON;
    }
}
