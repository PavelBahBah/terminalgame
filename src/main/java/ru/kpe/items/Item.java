package ru.kpe.items;

import ru.kpe.game.World;

public abstract class Item {

    public void removeItself() {
        World.game.getSquad().getInventory().getItems().remove(this);
    }

    public abstract void useItem();
    public abstract String getItemTitle();
    public abstract Rarity getItemRarity();

}
