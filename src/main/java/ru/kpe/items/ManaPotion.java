package ru.kpe.items;

public class ManaPotion extends Item {

    @Override
    public void useItem() {
        removeItself();
    }

    @Override
    public String getItemTitle() {
        return "Зелье маны";
    }

    @Override
    public Rarity getItemRarity() {
        return Rarity.COMMON;
    }
}
