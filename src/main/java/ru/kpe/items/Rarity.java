package ru.kpe.items;

public enum Rarity {

    COMMON,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY

}
