package ru.kpe.locations;

import ru.kpe.npcs.NPC;

import java.util.List;

public class BattleLocation extends Location {

    public BattleLocation(List<NPC> NPCs) {
        super(NPCs);
    }

    @Override
    public String getLocationTitle() {
        return "Локация для битв";
    }

    @Override
    public boolean isBattleLocation() {
        return true;
    }

}
