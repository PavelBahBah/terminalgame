package ru.kpe.locations;

import ru.kpe.npcs.NPC;

import java.util.List;

public abstract class Location {

    private List<Location> linkedLocations;
    private List<NPC> NPCs;

    public Location(List<NPC> NPCs) {
        this.NPCs = NPCs;
    }

    public abstract String getLocationTitle();
    public abstract boolean isBattleLocation();

    public List<Location> getLinkedLocations() {
        return linkedLocations;
    }

    public void setLinkedLocations(List<Location> linkedLocations) {
        this.linkedLocations = linkedLocations;
    }

    public List<NPC> getNPCs() {
        return NPCs;
    }

    public void setNPCs(List<NPC> NPCs) {
        this.NPCs = NPCs;
    }
}
