package ru.kpe.locations;

import ru.kpe.npcs.Innkeeper;
import ru.kpe.npcs.Mentor;

import java.util.Arrays;
import java.util.Collections;

public final class LocationList {

    private LocationList() {}

    public static final Location START_LOCATION = new StartLocation(
            Arrays.asList(new Innkeeper(), new Mentor())
    );
    public static final Location BATTLE_LOCATION = new BattleLocation(Collections.emptyList());

    static {
        START_LOCATION.setLinkedLocations(Collections.singletonList(BATTLE_LOCATION));
        BATTLE_LOCATION.setLinkedLocations(Collections.singletonList(START_LOCATION));
    }

}
