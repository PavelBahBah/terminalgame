package ru.kpe.locations;

import ru.kpe.npcs.NPC;

import java.util.List;

public class StartLocation extends Location {

    public StartLocation(List<NPC> NPCs) {
        super(NPCs);
    }

    @Override
    public String getLocationTitle() {
        return "Стартовая локация";
    }

    @Override
    public boolean isBattleLocation() {
        return false;
    }

}
