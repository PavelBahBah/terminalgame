package ru.kpe.combatants;

public abstract class Combatant {

    private Integer hp;
    private Integer maxHp;

    public abstract String getCombatantInfo();

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(Integer maxHp) {
        this.maxHp = maxHp;
    }

}
