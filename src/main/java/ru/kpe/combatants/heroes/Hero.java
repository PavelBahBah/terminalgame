package ru.kpe.combatants.heroes;

import ru.kpe.abilities.Ability;
import ru.kpe.combatants.Combatant;

import java.util.ArrayList;
import java.util.List;

public abstract class Hero extends Combatant {

    private List<Ability> abilities = new ArrayList<>(4);
    private Integer level;

    public abstract String getHeroName();

    public String getCombatantInfo() {
        return getHeroName() + " - " + getLevel() + " уровень. HP: " + getHp() + "/" + getMaxHp();
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
