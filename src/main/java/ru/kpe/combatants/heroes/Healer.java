package ru.kpe.combatants.heroes;

import ru.kpe.abilities.AbilityList;

public class Healer extends Hero {

    public Healer() {
        getAbilities().add(AbilityList.HEAL);
        getAbilities().add(AbilityList.ATTACK);
        setMaxHp(20);
        setHp(20);
        setLevel(1);
    }

    @Override
    public String getHeroName() {
        return "Лекарь";
    }
}
