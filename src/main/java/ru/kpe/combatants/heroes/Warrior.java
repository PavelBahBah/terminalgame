package ru.kpe.combatants.heroes;

import ru.kpe.abilities.AbilityList;

public class Warrior extends Hero {

    public Warrior() {
        getAbilities().add(AbilityList.ATTACK);
        setMaxHp(30);
        setHp(30);
        setLevel(1);
    }

    @Override
    public String getHeroName() {
        return "Воин";
    }
}
