package ru.kpe.combatants.enemies;

import ru.kpe.abilities.AbilityList;
import ru.kpe.game.BattleLogAction;
import ru.kpe.game.World;

import java.util.Random;

public class Skeleton extends Enemy {

    public Skeleton() {
        getAbilities().add(AbilityList.SKIP);
        setMaxHp(20);
        setHp(20);
    }

    @Override
    public String getEnemyName() {
        return "Скелет";
    }

    @Override
    public void processTurn() {
        BattleLogAction battleLogAction = new BattleLogAction(
                World.game.getBattle().getCurrentCombatant(),
                BattleLogAction.BattleLogActionType.ABILITY,
                World.game.getSquad().getHeroes().get(new Random().nextInt(World.game.getSquad().getHeroes().size())),
                AbilityList.ATTACK
        );
        World.game.getBattle().getBattleLog().add(battleLogAction);
    }
}
