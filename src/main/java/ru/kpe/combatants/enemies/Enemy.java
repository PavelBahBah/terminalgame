package ru.kpe.combatants.enemies;

import ru.kpe.abilities.Ability;
import ru.kpe.combatants.Combatant;

import java.util.ArrayList;
import java.util.List;

public abstract class Enemy extends Combatant {

    private List<Ability> abilities = new ArrayList<>(4);

    public abstract String getEnemyName();

    public abstract void processTurn();

    public String getCombatantInfo() {
        return getEnemyName() + ". HP: " + getHp() + "/" + getMaxHp();
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

}
